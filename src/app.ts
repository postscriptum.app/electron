import * as fs from "node:fs";
import * as path from "node:path";
import * as os from "node:os";
import {
	createArgsParser,
	createCommandConfig,
	createLogConsole,
	createPreloadConfig,
	createPreloadSource,
	isBasedOn,
	loadHyph,
	loadPlugin,
	timeoutRace
} from "@postscriptum.app/cli-common";
import {ElectronTarget} from "./target.js";
import type {CommandConfig} from "@postscriptum.app/cli-common";
import type * as Electron from "electron";
import type {Package} from "normalize-package-data";

const {app, ipcMain, protocol, net} = require("electron") as typeof Electron;

const argvIdx = process.env['PS_ARGV_IDX'] ? parseInt(process.env['PS_ARGV_IDX']) : (app.isPackaged ? 1 : 2);
const tempDir = fs.mkdtempSync(path.join(os.tmpdir(), 'postscriptum-electron-'));
const preloadPath = path.join(tempDir, 'preload.js');

process.env['ELECTRON_DISABLE_SECURITY_WARNINGS'] = "true";

app.on("ready", async () => {
	let config: CommandConfig = null;
	let logConsole: typeof console = null;
	try {
		const pkgData: Package = require(`${__dirname}/../package.json`);
		const argsParser = createArgsParser(pkgData.version, process.argv.slice(argvIdx), (argsParser) => {
			// commander 13.1, unknown options are considered arguments
			argsParser.allowUnknownOption().allowExcessArguments();
		});

		const coreModulePath = require.resolve('@postscriptum.app/core');
		const corePath = path.dirname(path.dirname(coreModulePath));
		const pluginsPaths =  [ path.resolve(corePath, `dist/plugins`), process.cwd() ];
		config = await createCommandConfig({corePath, pluginsPaths}, argsParser);
		logConsole = createLogConsole(config.logFile);
	} catch (e) {
		console.error(e);
	}

	try {
		const devMode = !config.output;

		if (config.isolatedFileAccess) {
			let firstRequest = true;
			protocol.handle('file', (req) => {
				if (!firstRequest && !isBasedOn(req.url, config.input)) return Response.error();
				if (firstRequest) firstRequest = false;
				return net.fetch(req.url, { bypassCustomProtocolHandlers: true });
			});
		}

		ipcMain.handle('psLoadPlugin', async (event, name: string) => loadPlugin(name, config.pluginsPaths));
		ipcMain.handle('psLoadHyph', async (event, lang: string) => loadHyph(lang, config.corePath));
		const bridgeSource = /* language=js */ `const { ipcRenderer } = require('electron');
postscriptumBridge = {
	loadPlugin: async (pluginName) => {
		const plugin = await ipcRenderer.invoke('psLoadPlugin', pluginName);
		const script = document.createElement('script');
		script.textContent = plugin;
		document.head.appendChild(script);
	},
	loadHyph: async (lang) => {
		return await ipcRenderer.invoke('psLoadHyph', lang);
	}
}
/* TODO, deprecated and to be removed */
postscriptumLoadPlugin = postscriptumBridge.loadPlugin;
`;

		const preloadConfig = await createPreloadConfig(config, !devMode);
		const preloadSource = await createPreloadSource(preloadConfig);
		await fs.promises.writeFile(preloadPath, bridgeSource + preloadSource);

		const target = new ElectronTarget({
			preloadPath,
			devTools: devMode,
			logConsole
		});
		await target.initialized;
		let exitCode = 0;
		try {
			await timeoutRace((async () => {
				const result = await target.process(config.input);
				if (!devMode) {
					const outputStream = config.output == "-" ? process.stdout : fs.createWriteStream(config.output);
					await target.print(outputStream, result);
				}
			}), config.timeout);
		} catch (e) {
			logConsole.error(e);
			exitCode = 1;
		} finally {
			if (!devMode) {
				await target.close();
				app.exit(exitCode);
			}
		}
	} catch (e) {
		logConsole.error(e);
		app.exit(1);
	}
});

app.on("quit", () => {
	fs.rmSync(tempDir, {recursive: true, force: true});
});
