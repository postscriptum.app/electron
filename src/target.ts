import {postPDF} from "@postscriptum.app/postpdf";
import type {Writable} from "node:stream";
import {defaultFontFamilies, type ErrorResult, type ProcessResult} from "@postscriptum.app/cli-common";
import type {Package} from "normalize-package-data";

// eslint-disable-next-line @typescript-eslint/naming-convention
const {BrowserWindow} = require("electron") as typeof Electron;

export interface ElectronTargetConfig {
	devTools: boolean;
	preloadPath: string;
	logConsole?: typeof console;
}

const LOG_LEVELS: Array<keyof Console> = ["debug", "info", "warn", "error"];

export class ElectronTarget {
	win: Electron.BrowserWindow;

	readonly initialized: Promise<void>;

	constructor(config: ElectronTargetConfig) {
		const logConsole = config.logConsole || console;
		this.initialized = new Promise<void>((resolve, reject) => {
			this.win = new BrowserWindow({
				width: 794,
				height: 1123,
				show: false,
				webPreferences: {
					devTools: config.devTools,
					preload: config.preloadPath,
					contextIsolation: false,
					defaultFontFamily: defaultFontFamilies
				}
			});

			this.win.webContents.once('preload-error', (ev, preloadPath, error) => {
				reject(error);
			});

			this.win.webContents.on('console-message', (ev, level, message) => {
				const logMethod = logConsole[LOG_LEVELS[level]] as (...data: any[]) => void;
				logMethod(message);
			});

			if (config.devTools) {
				this.win.show();
				this.win.webContents.openDevTools();
			}

			resolve();
		});
	}

	close(): Promise<void> {
		this.win.destroy();
		return new Promise((resolve) => {
			this.win.once('closed', resolve);
		});
	}

	async process(url: string): Promise<ProcessResult> {
		this.win.loadURL(url);
		const result = (await this.win.webContents.executeJavaScript('window.postscriptumPreload.done')) as ProcessResult | ErrorResult;
		if ('type' in result && result.type == 'error') {
			const error = new Error(result.message);
			error.stack = result.stack;
			throw error;
		}
		const pkgData: Package = require(`${__dirname}/../package.json`);
		(result as ProcessResult).info.producer = `Postscriptum/Electron ${pkgData.version}`;
		return result as ProcessResult;
	}

	async print(outputStream: Writable, result: ProcessResult): Promise<void> {
		const pdfData = await this.win.webContents.printToPDF({
			preferCSSPageSize: true,
			pageSize: "A4",
			printBackground: true,
			generateTaggedPDF: true
		});
		//await new Promise<void>((resolve) => outputStream.end(pdfData, resolve));
		await postPDF(pdfData, (result as ProcessResult), outputStream);
	}

}
